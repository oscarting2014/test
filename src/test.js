const https = require('https')
const MongoClient = require('mongodb').MongoClient

const options = {
  hostname: 'api.openweathermap.org',
  path: '/data/2.5/weather?q=HongKong&appid=507e3ff5da3d34dacbbc397e076130a1',
  method: 'GET'
}

const uri = "mongodb+srv://oscar:tkbfyW9YlItVk8li@cluster0.l3ytc.mongodb.net/testing?retryWrites=true&w=majority"
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true })

client.connect(err => {
  const collection = client.db("testing").collection("weathers")
  const req = https.request(options, res => {
    res.on('data', d => {
      var object = JSON.parse(d)
      console.log(object['weather'][0]['main'])
      var myobj = { weather:object['weather'][0]['main'], date:new Date(Date.now()) }
      collection.insertOne(myobj, function(err, res) {
        if (err) throw err
        client.close()
      })
    })
  })

  req.on('error', error => {
    console.log("OpenWeatherMap is down")
    console.log("Retrive latest weather from db")
    collection.findOne(
      {},
      { sort: { date: -1 } },
      (err, data) => {
       console.log(data['weather'])
       client.close()
      },
    )
  })

  req.end()
})
